package models.model.fuel.fuelSubTypes

import models.model.fuel.fuelTypes.FuelTypes
import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._
import slick.model.ForeignKeyAction

case class FuelSubType(fcatId: Int, fcatCode: String, fcatName: String, fcatFtpId: Int)

/** Table description of table ug_fuel_subtypes. Objects of this class serve as prototypes for rows in queries. */
class FuelSubTypes(_tableTag: Tag) extends Table[FuelSubType](_tableTag, Some(DbUtils.dbSchema), "ug_fuel_subtypes") {
  def * = (fcatId, fcatCode, fcatName, fcatFtpId) <> (FuelSubType.tupled, FuelSubType.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(fcatId), Rep.Some(fcatCode), Rep.Some(fcatName), Rep.Some(fcatFtpId)).shaped.<>({r=>import r._; _1.map(_=> FuelSubType.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column fcat_id SqlType(serial), AutoInc, PrimaryKey */
  val fcatId: Rep[Int] = column[Int]("fcat_id", O.AutoInc, O.PrimaryKey)
  /** Database column fcat_code SqlType(bpchar), Length(8,false) */
  val fcatCode: Rep[String] = column[String]("fcat_code", O.Length(8,varying=false))
  /** Database column fcat_name SqlType(text) */
  val fcatName: Rep[String] = column[String]("fcat_name")
  /** Database column fcat_ftp_id SqlType(int4) */
  val fcatFtpId: Rep[Int] = column[Int]("fcat_ftp_id")

  lazy val FuelTypes = new TableQuery(tag => new FuelTypes(tag))

  /** Foreign key referencing UgFuelTypes (database name foreign_key1) */
  lazy val FuelTypesFk = foreignKey("foreign_key1", fcatFtpId, FuelTypes)(r => r.ftpId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

  /** Uniqueness Index over (fcatCode) (database name ug_fuel_categories_fcat_code_key) */
  val index1 = index("ug_fuel_categories_fcat_code_key", fcatCode, unique=true)
}