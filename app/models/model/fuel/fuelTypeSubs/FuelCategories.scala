package models.model.fuel.fuelTypeSubs

import models.model.fuel.fuelTypes.FuelTypes
import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._
import slick.model.ForeignKeyAction

case class FuelTypeSub(subId: Int, mainTypeId: Int, subTypeId: Int)

/** Table description of table ug_fuel_type_subs. Objects of this class serve as prototypes for rows in queries. */
class FuelTypeSubs(_tableTag: Tag) extends Table[FuelTypeSub](_tableTag, Some(DbUtils.dbSchema), "ug_fuel_type_subs") {
  def * = (subId, mainTypeId, subTypeId) <> (FuelTypeSub.tupled, FuelTypeSub.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(subId), Rep.Some(mainTypeId), Rep.Some(subTypeId)).shaped.<>({r=>import r._; _1.map(_=> FuelTypeSub.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column sub_id SqlType(serial), AutoInc, PrimaryKey */
  val subId: Rep[Int] = column[Int]("sub_id", O.AutoInc, O.PrimaryKey)
  /** Database column main_type_id SqlType(int4) */
  val mainTypeId: Rep[Int] = column[Int]("main_type_id")
  /** Database column sub_type_id SqlType(int4) */
  val subTypeId: Rep[Int] = column[Int]("sub_type_id")

  lazy val FuelTypes = new TableQuery(tag => new FuelTypes(tag))

  /** Foreign key referencing UgFuelTypes (database name foreign_key1) */
  lazy val ugFuelTypesFk1 = foreignKey("foreign_key1", mainTypeId, FuelTypes)(r => r.ftpId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgFuelTypes (database name foreign_key2) */
  lazy val ugFuelTypesFk2 = foreignKey("foreign_key2", subTypeId, FuelTypes)(r => r.ftpId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}