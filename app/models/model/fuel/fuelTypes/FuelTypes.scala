package models.model.fuel.fuelTypes

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class FuelType(ftpId: Int, ftpCode: String, ftpName: String)

/** Table description of table ug_fuel_types. Objects of this class serve as prototypes for rows in queries. */
class FuelTypes(_tableTag: Tag) extends Table[FuelType](_tableTag, Some(DbUtils.dbSchema), "ug_fuel_types") {
  def * = (ftpId, ftpCode, ftpName) <> (FuelType.tupled, FuelType.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(ftpId), Rep.Some(ftpCode), Rep.Some(ftpName)).shaped.<>({r=>import r._; _1.map(_=> FuelType.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column ftp_id SqlType(serial), AutoInc, PrimaryKey */
  val ftpId: Rep[Int] = column[Int]("ftp_id", O.AutoInc, O.PrimaryKey)
  /** Database column ftp_code SqlType(bpchar), Length(8,false) */
  val ftpCode: Rep[String] = column[String]("ftp_code", O.Length(8,varying=false))
  /** Database column ftp_name SqlType(text) */
  val ftpName: Rep[String] = column[String]("ftp_name")

  /** Uniqueness Index over (ftpCode) (database name ug_fuel_types_ftp_code_key) */
  val index1 = index("ug_fuel_types_ftp_code_key", ftpCode, unique=true)
}
