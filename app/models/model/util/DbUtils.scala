package models.model.util
import play.Play

object DbUtils {
   def dbSchema = Play.application().configuration().getString("slick.dbs.default.db.schema")
}
