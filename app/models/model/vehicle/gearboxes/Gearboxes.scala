package models.model.vehicle.gearboxes

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class Gearbox(gbxId: Int, gbxType: String, gbxCode: String)

/** Table description of table ug_vehicle_gearbox. Objects of this class serve as prototypes for rows in queries. */
class Gearboxes(_tableTag: Tag) extends Table[Gearbox](_tableTag, Some(DbUtils.dbSchema), "ug_vehicle_gearbox") {
  def * = (gbxId, gbxType, gbxCode) <> (Gearbox.tupled, Gearbox.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(gbxId), Rep.Some(gbxType), Rep.Some(gbxCode)).shaped.<>({r=>import r._; _1.map(_=> Gearbox.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column gbx_id SqlType(serial), AutoInc, PrimaryKey */
  val gbxId: Rep[Int] = column[Int]("gbx_id", O.AutoInc, O.PrimaryKey)
  /** Database column gbx_type SqlType(text) */
  val gbxType: Rep[String] = column[String]("gbx_type")
  /** Database column gbx_code SqlType(bpchar), Length(4,false) */
  val gbxCode: Rep[String] = column[String]("gbx_code", O.Length(4,varying=false))

  /** Uniqueness Index over (gbxCode) (database name ug_vehicle_gearbox_gbx_code_key) */
  val index1 = index("ug_vehicle_gearbox_gbx_code_key", gbxCode, unique=true)
}