package models.model.vehicle.drivetrains

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._


case class Drivetrain(dtnId: Int, dtnCode: String, dtnDesc: String)

/** Table description of table ug_vehicle_drivetrain. Objects of this class serve as prototypes for rows in queries. */
class Drivetrains(_tableTag: Tag) extends Table[Drivetrain](_tableTag, Some(DbUtils.dbSchema), "ug_vehicle_drivetrain") {
  def * = (dtnId, dtnCode, dtnDesc) <> (Drivetrain.tupled, Drivetrain.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(dtnId), Rep.Some(dtnCode), Rep.Some(dtnDesc)).shaped.<>({r=>import r._; _1.map(_=> Drivetrain.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column dtn_id SqlType(serial), AutoInc, PrimaryKey */
  val dtnId: Rep[Int] = column[Int]("dtn_id", O.AutoInc, O.PrimaryKey)
  /** Database column dtn_code SqlType(bpchar), Length(8,false) */
  val dtnCode: Rep[String] = column[String]("dtn_code", O.Length(8,varying=false))
  /** Database column dtn_desc SqlType(text) */
  val dtnDesc: Rep[String] = column[String]("dtn_desc")

  /** Uniqueness Index over (dtnCode) (database name ug_vehicle_drivetrain_dtn_code_key) */
  val index1 = index("ug_vehicle_drivetrain_dtn_code_key", dtnCode, unique=true)
}