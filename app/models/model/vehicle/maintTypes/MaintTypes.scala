package models.model.vehicle.maintTypes

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class MaintType(mtypeId: Int, mytypeCode: String, mtypeName: String, mtypeDesc: String)

/** Table description of table ug_vehicle_maint_type. Objects of this class serve as prototypes for rows in queries. */
class MaintTypes(_tableTag: Tag) extends Table[MaintType](_tableTag, Some(DbUtils.dbSchema), "ug_vehicle_maint_type") {
  def * = (mtypeId, mytypeCode, mtypeName, mtypeDesc) <> (MaintType.tupled, MaintType.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(mtypeId), Rep.Some(mytypeCode), Rep.Some(mtypeName), Rep.Some(mtypeDesc)).shaped.<>({r=>import r._; _1.map(_=> MaintType.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column mtype_id SqlType(serial), AutoInc, PrimaryKey */
  val mtypeId: Rep[Int] = column[Int]("mtype_id", O.AutoInc, O.PrimaryKey)
  /** Database column mytype_code SqlType(bpchar), Length(8,false) */
  val mytypeCode: Rep[String] = column[String]("mytype_code", O.Length(8,varying=false))
  /** Database column mtype_name SqlType(text) */
  val mtypeName: Rep[String] = column[String]("mtype_name")
  /** Database column mtype_desc SqlType(text) */
  val mtypeDesc: Rep[String] = column[String]("mtype_desc")

  /** Uniqueness Index over (mytypeCode) (database name ug_vehicle_maint_type_mytype_code_key) */
  val index1 = index("ug_vehicle_maint_type_mytype_code_key", mytypeCode, unique=true)
}