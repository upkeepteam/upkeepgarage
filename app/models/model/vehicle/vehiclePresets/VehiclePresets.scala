package models.model.vehicle.vehiclePresets

import models.model.fuel.fuelTypes.FuelTypes
import models.model.util.DbUtils
import models.model.vehicle.bodies.VehicleBodies
import models.model.vehicle.drivetrains.Drivetrains
import models.model.vehicle.gearboxes.Gearboxes
import models.model.vehicle.vehicleModels.VehicleModels
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class VehiclePreset(vprsId: Int, vprsVmdlId: Int, vprsPower: String, vprsDtId: Int, vprsVbdId: Int, vprsGbxId: Int, vprsFtpId: Int)

/** Table description of table ug_vehicle_presets. Objects of this class serve as prototypes for rows in queries. */
class VehiclePresets(_tableTag: Tag) extends Table[VehiclePreset](_tableTag, Some(DbUtils.dbSchema), "ug_vehicle_presets") {
  def * = (vprsId, vprsVmdlId, vprsPower, vprsDtId, vprsVbdId, vprsGbxId, vprsFtpId) <> (VehiclePreset.tupled, VehiclePreset.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(vprsId), Rep.Some(vprsVmdlId), Rep.Some(vprsPower), Rep.Some(vprsDtId), Rep.Some(vprsVbdId), Rep.Some(vprsGbxId), Rep.Some(vprsFtpId)).shaped.<>({r=>import r._; _1.map(_=> VehiclePreset.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column vprs_id SqlType(serial), AutoInc, PrimaryKey */
  val vprsId: Rep[Int] = column[Int]("vprs_id", O.AutoInc, O.PrimaryKey)
  /** Database column vprs_vmdl_id SqlType(int4) */
  val vprsVmdlId: Rep[Int] = column[Int]("vprs_vmdl_id")
  /** Database column vprs_power SqlType(text) */
  val vprsPower: Rep[String] = column[String]("vprs_power")
  /** Database column vprs_dt_id SqlType(int4) */
  val vprsDtId: Rep[Int] = column[Int]("vprs_dt_id")
  /** Database column vprs_vbd_id SqlType(int4) */
  val vprsVbdId: Rep[Int] = column[Int]("vprs_vbd_id")
  /** Database column vprs_gbx_id SqlType(int4) */
  val vprsGbxId: Rep[Int] = column[Int]("vprs_gbx_id")
  /** Database column vprs_ftp_id SqlType(int4) */
  val vprsFtpId: Rep[Int] = column[Int]("vprs_ftp_id")

  lazy val FuelTypes = new TableQuery(tag => new FuelTypes(tag))
  lazy val VehicleBodies = new TableQuery(tag => new VehicleBodies(tag))
  lazy val Drivetrains = new TableQuery(tag => new Drivetrains(tag))
  lazy val Gearboxes = new TableQuery(tag => new Gearboxes(tag))
  lazy val VehicleModels = new TableQuery(tag => new VehicleModels(tag))

  /** Foreign key referencing UgFuelTypes (database name foreign_key5) */
  lazy val ugFuelTypesFk = foreignKey("foreign_key5", vprsFtpId, FuelTypes)(r => r.ftpId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgVehicleBody (database name foreign_key3) */
  lazy val ugVehicleBodyFk = foreignKey("foreign_key3", vprsVbdId, VehicleBodies)(r => r.bdyId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgVehicleDrivetrain (database name foreign_key2) */
  lazy val ugVehicleDrivetrainFk = foreignKey("foreign_key2", vprsDtId, Drivetrains)(r => r.dtnId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgVehicleGearbox (database name foreign_key4) */
  lazy val ugVehicleGearboxFk = foreignKey("foreign_key4", vprsGbxId, Gearboxes)(r => r.gbxId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgVehicleModel (database name foreign_key1) */
  lazy val ugVehicleModelFk = foreignKey("foreign_key1", vprsVmdlId, VehicleModels)(r => r.mdlId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}