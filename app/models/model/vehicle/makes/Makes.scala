package models.model.vehicle.makes

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class VehicleMake(mkId: Int, mkCode: String, mkName: String)

/** Table description of table ug_vehicle_make. Objects of this class serve as prototypes for rows in queries. */
class VehicleMakes(_tableTag: Tag) extends Table[VehicleMake](_tableTag, Some(DbUtils.dbSchema), "ug_vehicle_make") {
  def * = (mkId, mkCode, mkName) <> (VehicleMake.tupled, VehicleMake.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(mkId), Rep.Some(mkCode), Rep.Some(mkName)).shaped.<>({r=>import r._; _1.map(_=> VehicleMake.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column mk_id SqlType(serial), AutoInc, PrimaryKey */
  val mkId: Rep[Int] = column[Int]("mk_id", O.AutoInc, O.PrimaryKey)
  /** Database column mk_code SqlType(bpchar), Length(8,false) */
  val mkCode: Rep[String] = column[String]("mk_code", O.Length(8,varying=false))
  /** Database column mk_name SqlType(text) */
  val mkName: Rep[String] = column[String]("mk_name")

  /** Uniqueness Index over (mkCode) (database name ug_vehicle_make_mk_code_key) */
  val index1 = index("ug_vehicle_make_mk_code_key", mkCode, unique=true)
}