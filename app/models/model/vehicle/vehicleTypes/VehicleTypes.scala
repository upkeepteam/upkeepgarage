package models.model.vehicle.vehicleTypes

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class VehicleType(vtpId: Int, vtpName: String, vtpCode: String)

/** Table description of table ug_vehicle_type. Objects of this class serve as prototypes for rows in queries. */
class VehicleTypes(_tableTag: Tag) extends Table[VehicleType](_tableTag, Some(DbUtils.dbSchema), "ug_vehicle_type") {
  def * = (vtpId, vtpName, vtpCode) <> (VehicleType.tupled, VehicleType.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(vtpId), Rep.Some(vtpName), Rep.Some(vtpCode)).shaped.<>({r=>import r._; _1.map(_=> VehicleType.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column vtp_id SqlType(serial), AutoInc, PrimaryKey */
  val vtpId: Rep[Int] = column[Int]("vtp_id", O.AutoInc, O.PrimaryKey)
  /** Database column vtp_name SqlType(text) */
  val vtpName: Rep[String] = column[String]("vtp_name")
  /** Database column vtp_code SqlType(bpchar), Length(4,false) */
  val vtpCode: Rep[String] = column[String]("vtp_code", O.Length(4,varying=false))

  /** Uniqueness Index over (vtpCode) (database name ug_vehicle_type_vtp_code_key) */
  val index1 = index("ug_vehicle_type_vtp_code_key", vtpCode, unique=true)
}