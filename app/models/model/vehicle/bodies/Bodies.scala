package models.model.vehicle.bodies

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class VehicleBody(bdyId: Int, bdyName: String, bdyCode: String)

/** Table description of table ug_vehicle_body. Objects of this class serve as prototypes for rows in queries. */
class VehicleBodies(_tableTag: Tag) extends Table[VehicleBody](_tableTag, Some(DbUtils.dbSchema), "ug_vehicle_body") {
  def * = (bdyId, bdyName, bdyCode) <> (VehicleBody.tupled, VehicleBody.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(bdyId), Rep.Some(bdyName), Rep.Some(bdyCode)).shaped.<>({r=>import r._; _1.map(_=> VehicleBody.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column bdy_id SqlType(serial), AutoInc, PrimaryKey */
  val bdyId: Rep[Int] = column[Int]("bdy_id", O.AutoInc, O.PrimaryKey)
  /** Database column bdy_name SqlType(text) */
  val bdyName: Rep[String] = column[String]("bdy_name")
  /** Database column bdy_code SqlType(bpchar), Length(4,false) */
  val bdyCode: Rep[String] = column[String]("bdy_code", O.Length(4,varying=false))

  /** Uniqueness Index over (bdyCode) (database name ug_vehicle_body_bdy_code_key) */
  val index1 = index("ug_vehicle_body_bdy_code_key", bdyCode, unique=true)
}