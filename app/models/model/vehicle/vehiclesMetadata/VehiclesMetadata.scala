package models.model.vehicle.vehiclesMetadata

import models.model.units.distance.DistanceUnits
import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class VehicleMetadata(vmtdId: Int, vmtdBoughtDistance: scala.math.BigDecimal, vmtdBoughtPrice: Double, vmtdBoughtDate: java.sql.Date, vmtdLicnesePlate: String, vmtdVin: Int, vmtdTankCapacity: scala.math.BigDecimal, vmtdUrban: scala.math.BigDecimal, vmtdExtraUrban: scala.math.BigDecimal, vmtdCombined: scala.math.BigDecimal, vmtdDistId: Int, vmtdImgUrl: Option[String] = None)

/** Table description of table ug_vehicle_metadata. Objects of this class serve as prototypes for rows in queries. */
class VehiclesMetadata(_tableTag: Tag) extends Table[VehicleMetadata](_tableTag, Some(DbUtils.dbSchema), "ug_vehicle_metadata") {
  def * = (vmtdId, vmtdBoughtDistance, vmtdBoughtPrice, vmtdBoughtDate, vmtdLicnesePlate, vmtdVin, vmtdTankCapacity, vmtdUrban, vmtdExtraUrban, vmtdCombined, vmtdDistId, vmtdImgUrl) <> (VehicleMetadata.tupled, VehicleMetadata.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(vmtdId), Rep.Some(vmtdBoughtDistance), Rep.Some(vmtdBoughtPrice), Rep.Some(vmtdBoughtDate), Rep.Some(vmtdLicnesePlate), Rep.Some(vmtdVin), Rep.Some(vmtdTankCapacity), Rep.Some(vmtdUrban), Rep.Some(vmtdExtraUrban), Rep.Some(vmtdCombined), Rep.Some(vmtdDistId), vmtdImgUrl).shaped.<>({r=>import r._; _1.map(_=> VehicleMetadata.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get, _9.get, _10.get, _11.get, _12)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column vmtd_id SqlType(serial), AutoInc, PrimaryKey */
  val vmtdId: Rep[Int] = column[Int]("vmtd_id", O.AutoInc, O.PrimaryKey)
  /** Database column vmtd_bought_distance SqlType(numeric) */
  val vmtdBoughtDistance: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("vmtd_bought_distance")
  /** Database column vmtd_bought_price SqlType(money) */
  val vmtdBoughtPrice: Rep[Double] = column[Double]("vmtd_bought_price")
  /** Database column vmtd_bought_date SqlType(date) */
  val vmtdBoughtDate: Rep[java.sql.Date] = column[java.sql.Date]("vmtd_bought_date")
  /** Database column vmtd_licnese_plate SqlType(bpchar), Length(8,false) */
  val vmtdLicnesePlate: Rep[String] = column[String]("vmtd_licnese_plate", O.Length(8,varying=false))
  /** Database column vmtd_VIN SqlType(int4) */
  val vmtdVin: Rep[Int] = column[Int]("vmtd_VIN")
  /** Database column vmtd_tank_capacity SqlType(numeric) */
  val vmtdTankCapacity: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("vmtd_tank_capacity")
  /** Database column vmtd_urban SqlType(numeric) */
  val vmtdUrban: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("vmtd_urban")
  /** Database column vmtd_extra_urban SqlType(numeric) */
  val vmtdExtraUrban: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("vmtd_extra_urban")
  /** Database column vmtd_combined SqlType(numeric) */
  val vmtdCombined: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("vmtd_combined")
  /** Database column vmtd_dist_id SqlType(int4) */
  val vmtdDistId: Rep[Int] = column[Int]("vmtd_dist_id")
  /** Database column vmtd_img_url SqlType(varchar), Length(255,true), Default(None) */
  val vmtdImgUrl: Rep[Option[String]] = column[Option[String]]("vmtd_img_url", O.Length(255,varying=true), O.Default(None))

  lazy val DistanceUnits = new TableQuery(tag => new DistanceUnits(tag))

  /** Foreign key referencing UgUnitDistance (database name foreign_key1) */
  lazy val ugUnitDistanceFk = foreignKey("foreign_key1", vmtdDistId, DistanceUnits)(r => r.distId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}