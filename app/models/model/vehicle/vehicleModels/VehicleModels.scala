package models.model.vehicle.vehicleModels

import models.model.util.DbUtils
import models.model.vehicle.makes.VehicleMakes
import models.model.vehicle.vehicleTypes.VehicleTypes
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class VehicleModel(mdlId: Int, mdlMkId: Int, mdlModel: String, mdlVtpId: Int)

/** Table description of table ug_vehicle_model. Objects of this class serve as prototypes for rows in queries. */
class VehicleModels(_tableTag: Tag) extends Table[VehicleModel](_tableTag, Some(DbUtils.dbSchema), "ug_vehicle_model") {
  def * = (mdlId, mdlMkId, mdlModel, mdlVtpId) <> (VehicleModel.tupled, VehicleModel.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(mdlId), Rep.Some(mdlMkId), Rep.Some(mdlModel), Rep.Some(mdlVtpId)).shaped.<>({r=>import r._; _1.map(_=> VehicleModel.tupled((_1.get, _2.get, _3.get, _4.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column mdl_id SqlType(serial), AutoInc, PrimaryKey */
  val mdlId: Rep[Int] = column[Int]("mdl_id", O.AutoInc, O.PrimaryKey)
  /** Database column mdl_mk_id SqlType(int4) */
  val mdlMkId: Rep[Int] = column[Int]("mdl_mk_id")
  /** Database column mdl_model SqlType(text) */
  val mdlModel: Rep[String] = column[String]("mdl_model")
  /** Database column mdl_vtp_id SqlType(int4) */
  val mdlVtpId: Rep[Int] = column[Int]("mdl_vtp_id")


  lazy val VehiclesMake = new TableQuery(tag => new VehicleMakes(tag))
  lazy val VehicleTypes = new TableQuery(tag => new VehicleTypes(tag))

  /** Foreign key referencing UgVehicleMake (database name foreign_key1) */
  lazy val ugVehicleMakeFk = foreignKey("foreign_key1", mdlMkId, VehiclesMake)(r => r.mkId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgVehicleType (database name foreign_key2) */
  lazy val ugVehicleTypeFk = foreignKey("foreign_key2", mdlVtpId, VehicleTypes)(r => r.vtpId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}