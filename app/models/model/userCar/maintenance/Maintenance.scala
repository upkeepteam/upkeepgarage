package models.model.userCar.maintenance

import models.model.user.userVehicle.UserVehicles
import models.model.util.DbUtils
import models.model.vehicle.maintTypes.MaintTypes
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class Maintenance(maintId: Int, maintUvclId: Int, maintDate: java.sql.Date, maintOdometer: scala.math.BigDecimal, maintMtypeId: Int, maintCost: Double, maintNote: Option[String] = None)

/** Table description of table ug_usercar_maintenance. Objects of this class serve as prototypes for rows in queries. */
class Maintenances(_tableTag: Tag) extends Table[Maintenance](_tableTag, Some(DbUtils.dbSchema), "ug_usercar_maintenance") {
  def * = (maintId, maintUvclId, maintDate, maintOdometer, maintMtypeId, maintCost, maintNote) <> (Maintenance.tupled, Maintenance.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(maintId), Rep.Some(maintUvclId), Rep.Some(maintDate), Rep.Some(maintOdometer), Rep.Some(maintMtypeId), Rep.Some(maintCost), maintNote).shaped.<>({r=>import r._; _1.map(_=> Maintenance.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column maint_id SqlType(serial), AutoInc, PrimaryKey */
  val maintId: Rep[Int] = column[Int]("maint_id", O.AutoInc, O.PrimaryKey)
  /** Database column maint_uvcl_id SqlType(int4) */
  val maintUvclId: Rep[Int] = column[Int]("maint_uvcl_id")
  /** Database column maint_date SqlType(date) */
  val maintDate: Rep[java.sql.Date] = column[java.sql.Date]("maint_date")
  /** Database column maint_odometer SqlType(numeric) */
  val maintOdometer: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("maint_odometer")
  /** Database column maint_mtype_id SqlType(int4) */
  val maintMtypeId: Rep[Int] = column[Int]("maint_mtype_id")
  /** Database column maint_cost SqlType(money) */
  val maintCost: Rep[Double] = column[Double]("maint_cost")
  /** Database column maint_note SqlType(text), Default(None) */
  val maintNote: Rep[Option[String]] = column[Option[String]]("maint_note", O.Default(None))

  lazy val UserVehicles = new TableQuery(tag => new UserVehicles(tag))
  lazy val VehicleMaintTypes = new TableQuery(tag => new MaintTypes(tag))

  /** Foreign key referencing UgUserVehicle (database name foreign_key1) */
  lazy val ugUserVehicleFk = foreignKey("foreign_key1", maintUvclId, UserVehicles)(r => r.uvclId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgVehicleMaintType (database name foreign_key2) */
  lazy val ugVehicleMaintTypeFk = foreignKey("foreign_key2", maintMtypeId, VehicleMaintTypes)(r => r.mtypeId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}