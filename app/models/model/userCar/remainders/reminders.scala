package models.model.userCar.remainders

import models.model.userCar.maintenance.Maintenances
import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._


case class Reminder(remId: Int, remMaintId: Option[Int] = None, remName: String, remOdometer: Option[scala.math.BigDecimal] = None, remOdometerWhen: Option[scala.math.BigDecimal] = None, remDate: Option[java.sql.Date] = None, remDateWhen: Option[java.sql.Date] = None, remNote: Option[String] = None)

/** Table description of table ug_usercar_reminders. Objects of this class serve as prototypes for rows in queries. */
class Reminders(_tableTag: Tag) extends Table[Reminder](_tableTag, Some(DbUtils.dbSchema), "ug_usercar_reminders") {
  def * = (remId, remMaintId, remName, remOdometer, remOdometerWhen, remDate, remDateWhen, remNote) <> (Reminder.tupled, Reminder.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(remId), remMaintId, Rep.Some(remName), remOdometer, remOdometerWhen, remDate, remDateWhen, remNote).shaped.<>({r=>import r._; _1.map(_=> Reminder.tupled((_1.get, _2, _3.get, _4, _5, _6, _7, _8)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column rem_id SqlType(serial), AutoInc, PrimaryKey */
  val remId: Rep[Int] = column[Int]("rem_id", O.AutoInc, O.PrimaryKey)
  /** Database column rem_maint_id SqlType(int4), Default(None) */
  val remMaintId: Rep[Option[Int]] = column[Option[Int]]("rem_maint_id", O.Default(None))
  /** Database column rem_name SqlType(text) */
  val remName: Rep[String] = column[String]("rem_name")
  /** Database column rem_odometer SqlType(numeric), Default(None) */
  val remOdometer: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("rem_odometer", O.Default(None))
  /** Database column rem_odometer_when SqlType(numeric), Default(None) */
  val remOdometerWhen: Rep[Option[scala.math.BigDecimal]] = column[Option[scala.math.BigDecimal]]("rem_odometer_when", O.Default(None))
  /** Database column rem_date SqlType(date), Default(None) */
  val remDate: Rep[Option[java.sql.Date]] = column[Option[java.sql.Date]]("rem_date", O.Default(None))
  /** Database column rem_date_when SqlType(date), Default(None) */
  val remDateWhen: Rep[Option[java.sql.Date]] = column[Option[java.sql.Date]]("rem_date_when", O.Default(None))
  /** Database column rem_note SqlType(text), Default(None) */
  val remNote: Rep[Option[String]] = column[Option[String]]("rem_note", O.Default(None))

  lazy val Maintenances = new TableQuery(tag => new Maintenances(tag))

  /** Foreign key referencing UgUsercarMaintenance (database name foreign_key1) */
  lazy val ugUsercarMaintenanceFk = foreignKey("foreign_key1", remMaintId, Maintenances)(r => Rep.Some(r.maintId), onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}