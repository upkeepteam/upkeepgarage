package models.model.userCar.fuelings

import models.model.fuel.fuelTypes.FuelTypes
import models.model.units.currencies.Currencies
import models.model.units.volume.VolumeUnits
import models.model.user.userVehicle.UserVehicles
import models.model.util.DbUtils
import models.model.vehicle.maintTypes.MaintTypes
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class Fueling(fuelingId: Int, fuelingUvclId: Int, fuelingDate: java.sql.Date, fuelingFtpId: Int, fuelingOdometer: scala.math.BigDecimal, fuelingTripodometer: scala.math.BigDecimal, fuelingQuantity: scala.math.BigDecimal, fuelingVolId: Int, fuelingCost: String, fuelingCurrId: Int, fuelingNote: Option[String] = None)

/** Table description of table ug_usercar_fuelings. Objects of this class serve as prototypes for rows in queries. */
class Fuelings(_tableTag: Tag) extends Table[Fueling](_tableTag, Some(DbUtils.dbSchema), "ug_usercar_fuelings") {
  def * = (fuelingId, fuelingUvclId, fuelingDate, fuelingFtpId, fuelingOdometer, fuelingTripodometer, fuelingQuantity, fuelingVolId, fuelingCost, fuelingCurrId, fuelingNote) <> (Fueling.tupled, Fueling.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(fuelingId), Rep.Some(fuelingUvclId), Rep.Some(fuelingDate), Rep.Some(fuelingFtpId), Rep.Some(fuelingOdometer), Rep.Some(fuelingTripodometer), Rep.Some(fuelingQuantity), Rep.Some(fuelingVolId), Rep.Some(fuelingCost), Rep.Some(fuelingCurrId), fuelingNote).shaped.<>({r=>import r._; _1.map(_=> Fueling.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get, _9.get, _10.get, _11)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column fueling_id SqlType(serial), AutoInc, PrimaryKey */
  val fuelingId: Rep[Int] = column[Int]("fueling_id", O.AutoInc, O.PrimaryKey)
  /** Database column fueling_uvcl_Id SqlType(int4) */
  val fuelingUvclId: Rep[Int] = column[Int]("fueling_uvcl_Id")
  /** Database column fueling_date SqlType(date) */
  val fuelingDate: Rep[java.sql.Date] = column[java.sql.Date]("fueling_date")
  /** Database column fueling_ftp_id SqlType(int4) */
  val fuelingFtpId: Rep[Int] = column[Int]("fueling_ftp_id")
  /** Database column fueling_odometer SqlType(numeric) */
  val fuelingOdometer: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("fueling_odometer")
  /** Database column fueling_tripodometer SqlType(numeric) */
  val fuelingTripodometer: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("fueling_tripodometer")
  /** Database column fueling_quantity SqlType(numeric) */
  val fuelingQuantity: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("fueling_quantity")
  /** Database column fueling_vol_id SqlType(int4) */
  val fuelingVolId: Rep[Int] = column[Int]("fueling_vol_id")
  /** Database column fueling_cost SqlType(macaddr), Length(2147483647,false) */
  val fuelingCost: Rep[String] = column[String]("fueling_cost", O.Length(2147483647,varying=false))
  /** Database column fueling_curr_id SqlType(int4) */
  val fuelingCurrId: Rep[Int] = column[Int]("fueling_curr_id")
  /** Database column fueling_note SqlType(text), Default(None) */
  val fuelingNote: Rep[Option[String]] = column[Option[String]]("fueling_note", O.Default(None))

  lazy val FuelTypes = new TableQuery(tag => new FuelTypes(tag))
  lazy val Currencies = new TableQuery(tag => new Currencies(tag))
  lazy val VolumeUnits = new TableQuery(tag => new VolumeUnits(tag))
  lazy val UserVehicles = new TableQuery(tag => new UserVehicles(tag))

  /** Foreign key referencing UgFuelTypes (database name foreign_key2) */
  lazy val ugFuelTypesFk = foreignKey("foreign_key2", fuelingFtpId, FuelTypes)(r => r.ftpId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgUnitCurrency (database name foreign_key4) */
  lazy val ugUnitCurrencyFk = foreignKey("foreign_key4", fuelingCurrId, Currencies)(r => r.currId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgUnitVolume (database name foreign_key3) */
  lazy val ugUnitVolumeFk = foreignKey("foreign_key3", fuelingVolId, VolumeUnits)(r => r.volId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgUserVehicle (database name foreign_key1) */
  lazy val ugUserVehicleFk = foreignKey("foreign_key1", fuelingUvclId, UserVehicles)(r => r.uvclId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}