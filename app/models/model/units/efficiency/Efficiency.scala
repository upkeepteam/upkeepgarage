package models.model.units.efficiency

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class EfficiencyUnit(effId: Int, effCode: String, effName: String, effBase: Boolean, effBaseValue: scala.math.BigDecimal)

/** Table description of table ug_unit_efficiency. Objects of this class serve as prototypes for rows in queries. */
class EfficiencyUnits(_tableTag: Tag) extends Table[EfficiencyUnit](_tableTag, Some(DbUtils.dbSchema), "ug_unit_efficiency") {
  def * = (effId, effCode, effName, effBase, effBaseValue) <> (EfficiencyUnit.tupled, EfficiencyUnit.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(effId), Rep.Some(effCode), Rep.Some(effName), Rep.Some(effBase), Rep.Some(effBaseValue)).shaped.<>({r=>import r._; _1.map(_=> EfficiencyUnit.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column eff_id SqlType(serial), AutoInc, PrimaryKey */
  val effId: Rep[Int] = column[Int]("eff_id", O.AutoInc, O.PrimaryKey)
  /** Database column eff_code SqlType(bpchar), Length(8,false) */
  val effCode: Rep[String] = column[String]("eff_code", O.Length(8,varying=false))
  /** Database column eff_name SqlType(text) */
  val effName: Rep[String] = column[String]("eff_name")
  /** Database column eff_base SqlType(bool) */
  val effBase: Rep[Boolean] = column[Boolean]("eff_base")
  /** Database column eff_base_value SqlType(numeric) */
  val effBaseValue: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("eff_base_value")

  /** Uniqueness Index over (effCode) (database name ug_unit_efficiency_eff_code_key) */
  val index1 = index("ug_unit_efficiency_eff_code_key", effCode, unique=true)
}