package models.model.units.distance

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class DistanceUnit(distId: Int, distCode: String, distName: String, distBase: Boolean, distBaseValue: scala.math.BigDecimal)

/** Table description of table ug_unit_distance. Objects of this class serve as prototypes for rows in queries. */
class DistanceUnits(_tableTag: Tag) extends Table[DistanceUnit](_tableTag, Some("upkeep_garage"), "ug_unit_distance") {
  def * = (distId, distCode, distName, distBase, distBaseValue) <> (DistanceUnit.tupled, DistanceUnit.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(distId), Rep.Some(distCode), Rep.Some(distName), Rep.Some(distBase), Rep.Some(distBaseValue)).shaped.<>({r=>import r._; _1.map(_=> DistanceUnit.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column dist_id SqlType(serial), AutoInc, PrimaryKey */
  val distId: Rep[Int] = column[Int]("dist_id", O.AutoInc, O.PrimaryKey)
  /** Database column dist_code SqlType(bpchar), Length(8,false) */
  val distCode: Rep[String] = column[String]("dist_code", O.Length(8,varying=false))
  /** Database column dist_name SqlType(text) */
  val distName: Rep[String] = column[String]("dist_name")
  /** Database column dist_base SqlType(bool) */
  val distBase: Rep[Boolean] = column[Boolean]("dist_base")
  /** Database column dist_base_value SqlType(numeric) */
  val distBaseValue: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("dist_base_value")

  /** Uniqueness Index over (distCode) (database name ug_unit_distance_dist_code_key) */
  val index1 = index("ug_unit_distance_dist_code_key", distCode, unique=true)
}