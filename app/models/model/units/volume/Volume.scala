package models.model.units.volume

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class VolumeUnit(volId: Int, volCode: String, volName: String, volBase: Boolean, volBaseVal: scala.math.BigDecimal)

/** Table description of table ug_unit_volume. Objects of this class serve as prototypes for rows in queries. */
class VolumeUnits(_tableTag: Tag) extends Table[VolumeUnit](_tableTag, Some(DbUtils.dbSchema), "ug_unit_volume") {
  def * = (volId, volCode, volName, volBase, volBaseVal) <> (VolumeUnit.tupled, VolumeUnit.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(volId), Rep.Some(volCode), Rep.Some(volName), Rep.Some(volBase), Rep.Some(volBaseVal)).shaped.<>({r=>import r._; _1.map(_=> VolumeUnit.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column vol_id SqlType(serial), AutoInc, PrimaryKey */
  val volId: Rep[Int] = column[Int]("vol_id", O.AutoInc, O.PrimaryKey)
  /** Database column vol_code SqlType(bpchar), Length(8,false) */
  val volCode: Rep[String] = column[String]("vol_code", O.Length(8,varying=false))
  /** Database column vol_name SqlType(text) */
  val volName: Rep[String] = column[String]("vol_name")
  /** Database column vol_base SqlType(bool) */
  val volBase: Rep[Boolean] = column[Boolean]("vol_base")
  /** Database column vol_base_val SqlType(numeric) */
  val volBaseVal: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("vol_base_val")

  /** Uniqueness Index over (volCode) (database name ug_volume_unit_vol_unit_code_key) */
  val index1 = index("ug_volume_unit_vol_unit_code_key", volCode, unique=true)
}