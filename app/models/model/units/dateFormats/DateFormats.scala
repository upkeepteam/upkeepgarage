package models.model.units.dateFormats

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class DateFormat(dftId: Int, dftName: String, dftFormat: String)

/** Table description of table ug_unit_dateformat. Objects of this class serve as prototypes for rows in queries. */
class DateFormats(_tableTag: Tag) extends Table[DateFormat](_tableTag, Some(DbUtils.dbSchema), "ug_unit_dateformat") {
  def * = (dftId, dftName, dftFormat) <> (DateFormat.tupled, DateFormat.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(dftId), Rep.Some(dftName), Rep.Some(dftFormat)).shaped.<>({r=>import r._; _1.map(_=> DateFormat.tupled((_1.get, _2.get, _3.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column dft_id SqlType(serial), AutoInc, PrimaryKey */
  val dftId: Rep[Int] = column[Int]("dft_id", O.AutoInc, O.PrimaryKey)
  /** Database column dft_name SqlType(text) */
  val dftName: Rep[String] = column[String]("dft_name")
  /** Database column dft_format SqlType(text) */
  val dftFormat: Rep[String] = column[String]("dft_format")

  /** Uniqueness Index over (dftFormat) (database name ug_unit_dateformat_dft_format_key) */
  val index1 = index("ug_unit_dateformat_dft_format_key", dftFormat, unique=true)
  /** Uniqueness Index over (dftName) (database name ug_unit_dateformat_dft_name_key) */
  val index2 = index("ug_unit_dateformat_dft_name_key", dftName, unique=true)
}