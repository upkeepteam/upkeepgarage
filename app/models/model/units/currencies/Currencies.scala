package models.model.units.currencies

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class Currency(currId: Int, currCode: String, currName: String, currBase: Boolean, currBaseVal: scala.math.BigDecimal)

/** Table description of table ug_unit_currency. Objects of this class serve as prototypes for rows in queries. */
class Currencies(_tableTag: Tag) extends Table[Currency](_tableTag, Some(DbUtils.dbSchema), "ug_unit_currency") {
  def * = (currId, currCode, currName, currBase, currBaseVal) <> (Currency.tupled, Currency.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(currId), Rep.Some(currCode), Rep.Some(currName), Rep.Some(currBase), Rep.Some(currBaseVal)).shaped.<>({r=>import r._; _1.map(_=> Currency.tupled((_1.get, _2.get, _3.get, _4.get, _5.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column curr_id SqlType(serial), AutoInc, PrimaryKey */
  val currId: Rep[Int] = column[Int]("curr_id", O.AutoInc, O.PrimaryKey)
  /** Database column curr_code SqlType(bpchar), Length(8,false) */
  val currCode: Rep[String] = column[String]("curr_code", O.Length(8,varying=false))
  /** Database column curr_name SqlType(text) */
  val currName: Rep[String] = column[String]("curr_name")
  /** Database column curr_base SqlType(bool) */
  val currBase: Rep[Boolean] = column[Boolean]("curr_base")
  /** Database column curr_base_val SqlType(numeric) */
  val currBaseVal: Rep[scala.math.BigDecimal] = column[scala.math.BigDecimal]("curr_base_val")

  /** Uniqueness Index over (currCode) (database name ug_unit_currency_curr_code_key) */
  val index1 = index("ug_unit_currency_curr_code_key", currCode, unique=true)
}