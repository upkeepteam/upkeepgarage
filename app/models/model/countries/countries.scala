package models.model.countries

import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class Country(cntId: Int, cndCode: String, cntName: String, cntFlag: Option[String] = None)

/** Table description of table ug_countries. Objects of this class serve as prototypes for rows in queries. */
class Countries(_tableTag: Tag) extends Table[Country](_tableTag, Some(DbUtils.dbSchema), "ug_countries") {
  def * = (cntId, cndCode, cntName, cntFlag) <> (Country.tupled, Country.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(cntId), Rep.Some(cndCode), Rep.Some(cntName), cntFlag).shaped.<>({r=>import r._; _1.map(_=> Country.tupled((_1.get, _2.get, _3.get, _4)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column cnt_id SqlType(serial), AutoInc, PrimaryKey */
  val cntId: Rep[Int] = column[Int]("cnt_id", O.AutoInc, O.PrimaryKey)
  /** Database column cnd_code SqlType(bpchar), Length(8,false) */
  val cndCode: Rep[String] = column[String]("cnd_code", O.Length(8,varying=false))
  /** Database column cnt_name SqlType(text) */
  val cntName: Rep[String] = column[String]("cnt_name")
  /** Database column cnt_flag SqlType(text), Default(None) */
  val cntFlag: Rep[Option[String]] = column[Option[String]]("cnt_flag", O.Default(None))

  /** Uniqueness Index over (cndCode) (database name ug_countries_cnd_code_key) */
  val index1 = index("ug_countries_cnd_code_key", cndCode, unique=true)
}