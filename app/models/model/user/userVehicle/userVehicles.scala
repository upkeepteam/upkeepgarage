package models.model.user.userVehicle

import models.model.user.Users
import models.model.util.DbUtils
import models.model.vehicle.vehiclePresets.VehiclePresets
import models.model.vehicle.vehiclesMetadata.VehiclesMetadata
import slick.lifted.Tag
import slick.driver.PostgresDriver.api._

case class UserVehicle(uvclId: Int, uvclUserId: Int, uvclYear: java.sql.Date, uvclVprsId: Int, uvclModelDesc: String, uvclVmtdId: Int)

/** Table description of table ug_user_vehicle. Objects of this class serve as prototypes for rows in queries. */
class UserVehicles(_tableTag: Tag) extends Table[UserVehicle](_tableTag, Some(DbUtils.dbSchema), "ug_user_vehicle") {
  def * = (uvclId, uvclUserId, uvclYear, uvclVprsId, uvclModelDesc, uvclVmtdId) <> (UserVehicle.tupled, UserVehicle.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(uvclId), Rep.Some(uvclUserId), Rep.Some(uvclYear), Rep.Some(uvclVprsId), Rep.Some(uvclModelDesc), Rep.Some(uvclVmtdId)).shaped.<>({r=>import r._; _1.map(_=> UserVehicle.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column uvcl_id SqlType(serial), AutoInc, PrimaryKey */
  val uvclId: Rep[Int] = column[Int]("uvcl_id", O.AutoInc, O.PrimaryKey)
  /** Database column uvcl_user_id SqlType(int4) */
  val uvclUserId: Rep[Int] = column[Int]("uvcl_user_id")
  /** Database column uvcl_year SqlType(date) */
  val uvclYear: Rep[java.sql.Date] = column[java.sql.Date]("uvcl_year")
  /** Database column uvcl_vprs_id SqlType(int4) */
  val uvclVprsId: Rep[Int] = column[Int]("uvcl_vprs_id")
  /** Database column uvcl_model_desc SqlType(text) */
  val uvclModelDesc: Rep[String] = column[String]("uvcl_model_desc")
  /** Database column uvcl_vmtd_id SqlType(int4) */
  val uvclVmtdId: Rep[Int] = column[Int]("uvcl_vmtd_id")

  lazy val Users = new TableQuery(tag => new Users(tag))
  lazy val VehiclesMetadata = new TableQuery(tag => new VehiclesMetadata(tag))
  lazy val VehiclePresets = new TableQuery(tag => new VehiclePresets(tag))

  /** Foreign key referencing UgUsers (database name foreign_key1) */
  lazy val ugUsersFk = foreignKey("foreign_key1", uvclUserId, Users)(r => r.userId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgVehicleMetadata (database name foreign_key3) */
  lazy val ugVehicleMetadataFk = foreignKey("foreign_key3", uvclVmtdId, VehiclesMetadata)(r => r.vmtdId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgVehiclePresets (database name foreign_key2) */
  lazy val ugVehiclePresetsFk = foreignKey("foreign_key2", uvclVprsId, VehiclePresets)(r => r.vprsId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}