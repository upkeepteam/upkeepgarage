package models.model.user.userMetadata

import models.model.countries.Countries
import models.model.units.currencies.Currencies
import models.model.units.dateFormats.DateFormats
import models.model.units.distance.DistanceUnits
import models.model.units.efficiency.EfficiencyUnits
import models.model.units.volume.VolumeUnits
import models.model.util.DbUtils
import slick.lifted.{Tag}
import slick.driver.PostgresDriver.api._

case class UserMetadata(umdtId: Int, umdtLastActive: java.sql.Date, umdtCntId: Int, umdtRmndId: Int, umdtVlmId: Int, umdtEffId: Int, umdtDstId: Int, umdtCurrId: Int, umdtDtfId: Int, umdtImgUrl: Option[String] = None)

/** Table description of table ug_user_metadata. Objects of this class serve as prototypes for rows in queries. */
class UsersMetadata(_tableTag: Tag) extends Table[UserMetadata](_tableTag, Some(DbUtils.dbSchema), "ug_user_metadata") {
  def * = (umdtId, umdtLastActive, umdtCntId, umdtRmndId, umdtVlmId, umdtEffId, umdtDstId, umdtCurrId, umdtDtfId, umdtImgUrl) <> (UserMetadata.tupled, UserMetadata.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  def ? = (Rep.Some(umdtId), Rep.Some(umdtLastActive), Rep.Some(umdtCntId), Rep.Some(umdtRmndId), Rep.Some(umdtVlmId), Rep.Some(umdtEffId), Rep.Some(umdtDstId), Rep.Some(umdtCurrId), Rep.Some(umdtDtfId), umdtImgUrl).shaped.<>({r=>import r._; _1.map(_=> UserMetadata.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get, _8.get, _9.get, _10)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column umdt_id SqlType(serial), AutoInc, PrimaryKey */
  val umdtId: Rep[Int] = column[Int]("umdt_id", O.AutoInc, O.PrimaryKey)
  /** Database column umdt_last_active SqlType(date) */
  val umdtLastActive: Rep[java.sql.Date] = column[java.sql.Date]("umdt_last_active")
  /** Database column umdt_cnt_id SqlType(int4) */
  val umdtCntId: Rep[Int] = column[Int]("umdt_cnt_id")
  /** Database column umdt_rmnd_id SqlType(int4) */
  val umdtRmndId: Rep[Int] = column[Int]("umdt_rmnd_id")
  /** Database column umdt_vlm_id SqlType(int4) */
  val umdtVlmId: Rep[Int] = column[Int]("umdt_vlm_id")
  /** Database column umdt_eff_id SqlType(int4) */
  val umdtEffId: Rep[Int] = column[Int]("umdt_eff_id")
  /** Database column umdt_dst_id SqlType(int4) */
  val umdtDstId: Rep[Int] = column[Int]("umdt_dst_id")
  /** Database column umdt_curr_id SqlType(int4) */
  val umdtCurrId: Rep[Int] = column[Int]("umdt_curr_id")
  /** Database column umdt_dtf_Id SqlType(int4) */
  val umdtDtfId: Rep[Int] = column[Int]("umdt_dtf_Id")
  /** Database column umdt_img_url SqlType(varchar), Length(255,true), Default(None) */
  val umdtImgUrl: Rep[Option[String]] = column[Option[String]]("umdt_img_url", O.Length(255,varying=true), O.Default(None))

  lazy val Countries = new TableQuery(tag => new Countries(tag))
  lazy val Currencies = new TableQuery(tag => new Currencies(tag))
  lazy val DateFormats = new TableQuery(tag => new DateFormats(tag))
  lazy val DistanceUnits = new TableQuery(tag => new DistanceUnits(tag))
  lazy val EfficiencyUnits = new TableQuery(tag => new EfficiencyUnits(tag))
  lazy val VolumeUnits = new TableQuery(tag => new VolumeUnits(tag))

  /** Foreign key referencing UgCountries (database name foreign_key1) */
  lazy val ugCountriesFk = foreignKey("foreign_key1", umdtCntId, Countries)(r => r.cntId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgUnitCurrency (database name foreign_key5) */
  lazy val ugUnitCurrencyFk = foreignKey("foreign_key5", umdtCurrId, Currencies)(r => r.currId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgUnitDateformat (database name foreign_key6) */
  lazy val ugUnitDateformatFk = foreignKey("foreign_key6", umdtDtfId, DateFormats)(r => r.dftId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgUnitDistance (database name foreign_key4) */
  lazy val ugUnitDistanceFk = foreignKey("foreign_key4", umdtDstId, DistanceUnits)(r => r.distId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgUnitEfficiency (database name foreign_key3) */
  lazy val ugUnitEfficiencyFk = foreignKey("foreign_key3", umdtEffId, EfficiencyUnits)(r => r.effId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgUnitVolume (database name foreign_key2) */
  lazy val ugUnitVolumeFk = foreignKey("foreign_key2", umdtVlmId, VolumeUnits)(r => r.volId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
}