package models.model.user

import models.model.user.loginInfo.LoginInfo
import models.model.user.userMetadata.UsersMetadata
import models.model.util.DbUtils
import slick.lifted.Tag
import slick.driver.PostgresDriver.api._
import slick.model.ForeignKeyAction

case class User(userId: Option[Int], userLogMethod: String, userEmail: String, userStatus: Option[String], userName: String, userPassword: String, userUmdtId: Option[Int], userLogId: Int)
  extends scala.AnyRef with com.mohiva.play.silhouette.api.Identity with scala.Product with scala.Serializable

/** Table description of table ug_users. Objects of this class serve as prototypes for rows in queries. */
class Users(_tableTag: Tag) extends Table[User](_tableTag, Some(DbUtils.dbSchema), "ug_users") {
  def * = (userId?, userLogMethod, userEmail, userStatus?, userName, userPassword, userUmdtId?, userLogId) <> (User.tupled, User.unapply)
  /** Maps whole row to an option. Useful for outer joins. */
  //def ? = (Rep.Some(userId), Rep.Some(userLogMethod), Rep.Some(userEmail), Rep.Some(userStatus), Rep.Some(userName), Rep.Some(userPassword), Rep.Some(userUmdtId)).shaped.<>({r=>import r._; _1.map(_=> User.tupled((_1.get, _2.get, _3.get, _4.get, _5.get, _6.get, _7.get)))}, (_:Any) =>  throw new Exception("Inserting into ? projection not supported."))

  /** Database column user_id SqlType(serial), AutoInc, PrimaryKey */
  val userId: Rep[Int] = column[Int]("user_id", O.AutoInc, O.PrimaryKey)
  /** Database column user_log_method SqlType(bpchar), Length(3,false) */
  val userLogMethod: Rep[String] = column[String]("user_log_method", O.Length(3,varying=false))
  /** Database column user_email SqlType(text) */
  val userEmail: Rep[String] = column[String]("user_email")
  /** Database column user_status SqlType(bpchar), Length(3,false) */
  val userStatus: Rep[String] = column[String]("user_status", O.Length(3,varying=false))
  /** Database column user_name SqlType(text) */
  val userName: Rep[String] = column[String]("user_name")
  /** Database column user_password SqlType(text) */
  val userPassword: Rep[String] = column[String]("user_password")
  /** Database column user_umdt_id SqlType(int4) */
  val userUmdtId: Rep[Int] = column[Int]("user_umdt_id")
  /** Database column user_log_id SqlType(int4) */
  val userLogId: Rep[Int] = column[Int]("user_log_id")


  lazy val UsersMetadata = new TableQuery(tag => new UsersMetadata(tag))

  lazy val LoginInfo = new TableQuery(tag => new LoginInfo(tag))

  /** Foreign key referencing UgUserMetadata (database name foreign_key1) */
  lazy val ugUserMetadataFk = foreignKey("foreign_key1", userUmdtId, UsersMetadata)(r => r.umdtId, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)
  /** Foreign key referencing UgLoginInfo (database name foreign_key2) */
  lazy val ugLoginInfoFk = foreignKey("foreign_key2", userLogId, LoginInfo)(r => r.logid, onUpdate=ForeignKeyAction.NoAction, onDelete=ForeignKeyAction.NoAction)

  /** Uniqueness Index over (userEmail) (database name ug_users_user_email_key) */
  val index1 = index("ug_users_user_email_key", userEmail, unique=true)
  /** Uniqueness Index over (userName) (database name ug_users_user_name_key) */
  val index2 = index("ug_users_user_name_key", userName, unique=true)
  /** Uniqueness Index over (userPassword) (database name ug_users_user_password_key) */
  val index3 = index("ug_users_user_password_key", userPassword, unique=true)
}