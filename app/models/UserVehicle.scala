package models

case class UserVehicle(id: Long, name: String, mileage: String, quantity: String)

object  UserVehicle {
  var vehicleList = Set(
    UserVehicle(1,"Honda 2.2 I-CTDI", "6,20", "4 fuelings"),
    UserVehicle(2,"Abarth Fiat 500", "7,44", "9 fuelings"),
    UserVehicle(3,"Hyundai Accent 1,5 CRDi VGT Elegance", "4,76", "152 fuelings")
  )

  def findAll = vehicleList.toList
}