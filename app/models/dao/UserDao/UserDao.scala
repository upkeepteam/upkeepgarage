package models.dao.UserDao


import models.dao.baiscDao.BasicDao
import models.model.user.loginInfo.DBLoginInfo
import models.model.user.{Users, User}
import slick.driver.PostgresDriver.api._
import scala.concurrent.Future

class UserDao extends BasicDao {

  lazy val query = TableQuery[Users]

  def findAllUsers: Future[Seq[User]] = {
    dbConfig.db.run(query.result)
  }

  def findUserById(id: Int) = {
    dbConfig.db.run(query.filter(user => user.userId === id).result.headOption)
  }

  def findUserByEmail(email: String): Future[Option[User]] = {
    dbConfig.db.run(query.filter(user => user.userEmail === email).result.headOption)
  }

  def saveUser(user: User): Unit = {
    dbConfig.db.run(query += user)
  }

  def findUserByLoginInfo(logInfo: DBLoginInfo): Future[Option[User]] = {
    dbConfig.db.run(query.filter(_.userLogId === logInfo.logid ).result.headOption)
  }
}

