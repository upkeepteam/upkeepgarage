package models.dao.baiscDao


import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import slick.driver.JdbcProfile


trait BasicDao {
  /** Gaunamas dbConfig sql uzklausu paleidimui */
  val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
}
