package models.service.userService

import javax.inject.Inject

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService
import models.dao.UserDao.UserDao
import models.model.user.User

import scala.concurrent.Future

class UserService @Inject()(userDao: UserDao) extends IdentityService[User]{

  def find(id: Int): Future[Option[User]] = {
    userDao.findUserById(id)
  }

  def save(user: User): Unit = {
    userDao.saveUser(user)
  }

  override def retrieve(loginInfo: LoginInfo): Future[Option[User]] = {
    userDao.findUserByEmail(loginInfo.providerKey)
  }
}
