package forms

import play.api.data.Form
import play.api.data.Forms._

object SignUpForm {

  val singUpForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "password" -> nonEmptyText,
      "email" -> email
  )(signUpFromData.apply)(signUpFromData.unapply))

  case class signUpFromData(name: String, password: String, email: String)
}
