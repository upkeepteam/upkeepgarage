package controllers

import javax.inject.Inject

import models.dao.UserDao.UserDao
import play.api.i18n.{MessagesApi, I18nSupport}
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext

class Application @Inject() (val messagesApi: MessagesApi, val userDao: UserDao) extends
    Controller with I18nSupport {

  def index = Action.async {
    val futureResult = userDao.findAllUsers
    futureResult.map(users => Ok(views.html.index(users)))
  }

  def loginIndex = Action {
    Ok(views.html.login())
  }

  def login = Action {
    Redirect(routes.Application.index())
  }

  def singUp = Action {
    Ok(views.html.signUp())
  }
}