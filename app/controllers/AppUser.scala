package controllers


import javax.inject.Inject

import models.UserVehicle
import play.api.i18n.{Messages, MessagesApi, I18nSupport}
import play.api.mvc._

class AppUser  @Inject() (val messagesApi: MessagesApi) extends Controller with I18nSupport{
  def showMyGarage = Action {
    implicit  request =>
      val list = UserVehicle.findAll
      Ok(views.html.user.mygarage(list, Messages("myGarage.title")))
  }
}
